const express = require("express");
const router = express.Router();

// Tu auras besoin de ta connexion à la base de données pour faire les requêtes SQL
const connection = require("../config/db");

router.get("/", (req, res) => {
  connection.query("SELECT * FROM director", (err, results) => {
    if (err) {
      res.status(500).json({
        message: err.message,
        error: err,
      });
    } else {
      res.status(200).json(results);
    }
  });
});

router.post("/", (req, res) => {
  const data = req.body;
  const fullname = data.director_fullname;
  console.error("fullname", fullname)
  if (!fullname) { // truthy / fasly
    return res.status(400).json({
      error: 'director_fullname is empty'
    })
  }
  connection.execute(
    `
    INSERT INTO director (director_fullname)
    VALUES (?)
  `,
    [data.director_fullname],
    (err, results) => {
      if (err) {
        res.status(500).json({
          message: err.message,
          error: err,
        });
      } else {
        res.status(200).json({
          ...data,
          director_id: results.insertId
        });
      }
    }
  );
});

// TODO : faire le CRUD

module.exports = router;
