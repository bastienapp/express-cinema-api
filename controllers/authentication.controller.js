const express = require("express");
const router = express.Router();
const bcrypt = require("bcrypt");
const { v4: uuidv4 } = require('uuid');
const connection = require("../config/db");

router.post("/register", (req, res) => {
  const { email, password } = req.body;
  if (!email || !password) {
    res.status(400).json({
      error: "Email or password missing",
    });
  } else {
    // chiffrement (encrypage)
    // hachage (hashing)
    // salage (salting)
    const saltRounds = 10;
    bcrypt.hash(password, saltRounds, function (hashError, hashedPassword) {
      if (hashError) {
        res.status(500).json({
          error: hashError,
        });
      } else {
        // TODO vérifier si un compte n'existe pas déjà
        connection.execute(
          "INSERT INTO user (email, password) VALUES (?, ?)",
          [email, hashedPassword],
          (mysqlError, result) => {
            if (mysqlError) {
              res.status(500).json({
                error: mysqlError,
              });
            } else {
              res.status(201).json({
                userId: result.insertId,
                email: email,
              });
            }
          }
        );
      }
    });
  }
});

router.post("/login", (req, res) => {
  const { email, password } = req.body;
  if (!email || !password) {
    res.status(400).json({
      error: "Email or password missing",
    });
  } else {
    connection.execute(
      "SELECT * FROM user WHERE email = ?",
      [email],
      (mysqlEmailError, results) => {
        if (mysqlEmailError) {
          res.status(500).json({ error: mysqlEmailError });
        } else if (results.length === 0) {
          res.status(401).json({ error: "Invalid email" });
        } else {
          const user = results[0];
          const hashedPassword = user.password;
          bcrypt.compare(
            password,
            hashedPassword,
            function (hashError, passwordMatched) {
              if (hashError) {
                res.status(500).json({ error: hashError });
              } else if (!passwordMatched) {
                res.status(401).json({ error: "Invalid password" });
              } else {
                // créer un identifiant de session unique pour l'utilisateur
                const sessionId = uuidv4();
                /*
                  Pour l'utilisateur connecté, ajoute un identifiant de session dans sa ligne de la table user
                */
                connection.execute(
                  'UPDATE user SET session_id = ? WHERE user_id = ?',
                  [sessionId, user.user_id],
                  (err, results) => {
                    if (err) {
                      res.status(500).json({error: err})
                    } else {
                      // renvoyer la session à l'utilisateur
                      res.status(200).json({
                        email: user.email,
                        sessionId: sessionId
                      });
                    }
                  }
                );
              }
            }
          );
        }
      }
    );
  }
});

module.exports = router;
