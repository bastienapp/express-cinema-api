const express = require("express");
const router = express.Router();

// Tu auras besoin de ta connexion à la base de données pour faire les requêtes SQL
const connection = require("../config/db");

router.get("/", (req, res) => {
  // Exécution d'une requête SQL pour sélectionner tous les livres
  connection.query("SELECT * FROM movie", (err, results) => {
    // Gestion d'erreurs éventuelles lors de l'exécution de la requête
    if (err) {
      res.status(500).send(err.message);
    } else {
      // Envoi des résultats sous forme de JSON si la requête est réussie
      res.json(results);
    }
  });
});

router.get("/:movieId", (request, res) => {
  const movieId = request.params.movieId;
  // dans la base de données, trouver le film qui a l'identifiant movieId 2
  // requête SQL : SELECT * FROM movie WHERE movie_id = 2
  // Chloé : requête préparée pour éviter l'injection SQL
  connection.execute(
    `SELECT *
    FROM movie m
    INNER JOIN director d ON m.director_id = d.director_id
    WHERE movie_id = ?`,
    [movieId],
    (err, results) => {
      if (err) {
        res.status(500).json(err);
      } else {
        // si j'ai pas de résultat, affiche une erreur 404
        if (results.length === 0) {
          res.status(404).json({ error: `No movie found with id ${movieId}` });
        } else {
          res.json(results[0]);
        }
      }
    }
  );
});

// TODO récupérer les films par leur titre
router.get("/title/:title", (request, response) => {
  const title = request.params.title;
  connection.execute(
    "SELECT * FROM movie WHERE title LIKE ?",
    ["%" + title + "%"],
    (err, results) => {
      if (err) {
        response.json(err);
      } else {
        response.json(results);
      }
    }
  );
});

router.delete("/:movieId", (req, res) => {
  const id = req.params.movieId;

  connection.execute(
    "SELECT * FROM movie where movie_id = ?",
    [id],
    (err, results) => {
      if (err) {
        res.json(err);
      } else if (results.length === 0) {
        res.status(404).json({
          error: `Aucun film ${id} n'a été trouvé`,
        });
      } else {
        connection.execute(
          "DELETE FROM movie WHERE movie_id = ?",
          [id],
          (err, results) => {
            if (err) {
              res.json(err);
            } else {
              res.json({
                message: `Le film ${id} a bien été supprimé`,
              });
            }
          }
        );
      }
    }
  );
});

// cette route sert à ajouter un film à la base de données
router.post("/", function (request, response) {
  /*
    {
        title: "Shrek",
        release_year: 2001,
        rate: 9.5,
        duration: 90
    }
  */
  // const data = request.body;
  // ex: data.title
  const { title, release_year: releaseYear, rate, duration } = request.body;

  // Exécution d'une requête SQL pour insérer un nouveau livre
  connection.execute(
    "INSERT INTO movie (title, release_year, rate, duration) VALUES (?, ?, ?, ?)",
    [title, releaseYear, rate, duration],
    (err, results) => {
      if (err) {
        response.status(500).send(err.message);
      } else {
        // TODO améliorer le retour d'API
        /*
          {
              "title": "The Thing",
              "release_year": 1981,
              "rate": 8.2,
              "duration": 84
          }
          + "movie_id": 7,
        */
        // const newMovie = request.body;
        // newMovie.movie_id = results.insertId;
        const newMovie = {
          ...request.body,
          movie_id: results.insertId,
        };
        response.status(201).json(newMovie);
      }
    }
  );
});

router.put("/:movieId", (request, response) => {
  const id = request.params.movieId;
  const data = request.body;

  connection.execute(
    "SELECT * FROM movie where movie_id = ?",
    [id],
    (error, results) => {
      if (error) {
        response.status(500).json({
          error: error.message,
        });
      } else if (results.length === 0) {
        response.status(404).json({
          error: `Aucun film ${id} n'a été trouvé`,
        });
      } else {
        // modifier le film
        const movieToUpdate = results[0];
        // data contient les données à modifier
        const movieUpdated = {
          ...movieToUpdate,
          ...data,
        };
        connection.execute(
          "UPDATE movie SET title=?, release_year=?, duration=?, rate=? WHERE movie_id = ?",
          [
            movieUpdated.title,
            movieUpdated.release_year,
            movieUpdated.duration,
            movieUpdated.rate,
            id,
          ],
          (error, results) => {
            if (error) {
              response.status(500).json({
                error: error.message,
              });
            } else {
              response.status(200).json(movieUpdated);
            }
          }
        );
      }
    }
  );
});

// Route qui ajoute une catégorie à un film
// POST
router.post("/:movie_id/categories/:category_id", (req, res) => {
  const movieId = req.params.movie_id;
  const categoryId = req.params.category_id;

  // ce film a cette catégorie
  connection.execute(
    "INSERT INTO movie_has_categories (movie_id, category_id) VALUES (?, ?)",
    [movieId, categoryId],
    (err, results) => {
      if (err) {
        res.status(500).json({
          message: err.message,
          error: err,
        });
      } else {
        res.json(results);
      }
    }
  );
});

router.get("/directors/:director_name", (req, res) => {
  const directorName = req.params.director_name;

  // chercher les films dont le réalisateur a le nom director_name
  // movie
  connection.execute(
    `SELECT *
    FROM movie m
    INNER JOIN director d ON m.director_id = d.director_id
    WHERE director_fullname LIKE ?`,
    ['%' + directorName + '%'],
    (err, results) => {
      if (err) {
        res.status(500).json({
          message: err.message,
          error: err,
        });
      } else {
        res.json(results);
      }
    }
  );
});

module.exports = router;
