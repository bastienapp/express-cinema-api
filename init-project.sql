CREATE DATABASE express_cinema;

USE express_cinema;

CREATE USER 'Craving0006'@'localhost' IDENTIFIED BY 'ZA^Sg!kvB!4&Rw46S5Spov4FpTn4G837';

GRANT ALL PRIVILEGES ON express_cinema.* TO 'Craving0006'@'localhost';

FLUSH PRIVILEGES;

INSERT INTO movie (title, release_year, duration, rate)
	VALUES ("Barbie", 2023, 128, 10);

INSERT INTO movie (title, release_year, duration, rate)
	VALUES ("Blade Runner", 1982, 146, 10);

INSERT INTO movie (title, release_year, duration, rate)
	VALUES ("L'exorciste", 1973, 102, 9);

SELECT * FROM movie;