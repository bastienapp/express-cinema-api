const express = require("express");
const cors = require('cors');
const app = express();
require("dotenv").config();
const movieController = require('./controllers/movies.controller');
const directorController = require('./controllers/director.controller');
const authenticationController = require('./controllers/authentication.controller');

// pour récupérer les corps de requêtes JSON !
app.use(express.json()); // middleware

var corsOptions = {
  origin: process.env.FRONTEND_URL, // l'adresse du frontend (React)
}
app.use(cors(corsOptions));

const port = 5000;

app.use('/movies', movieController);
app.use('/directors', directorController);
app.use('/auth', authenticationController);

app.listen(port, () => {
  console.log(`http://localhost:${port}`);
});
