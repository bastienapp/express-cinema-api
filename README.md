# Cinema API

## Pré-requis

Installer Node.

## Initialisation du projet

Pour installer les dépendances, taper :

```bash
npm ci
```

## Configuration du projet

Initialiser les données de la base à partir du fichier `init-project.sql`.

Dupliquer le fichier `.env.sample` en `.env` et renseigner les valeurs attendues.