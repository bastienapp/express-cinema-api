USE flask_cinema;

CREATE TABLE director (
	director_id INT PRIMARY KEY AUTO_INCREMENT NOT NULL,
	director_fullname VARCHAR(255) NOT NULL
);

INSERT INTO director (director_fullname) VALUES
	('Michel Durand'),
	('Brandon'),
	('Gertrude');

SELECT * FROM director;

DELETE FROM director WHERE director_id IN (4, 5, 6);

SELECT * FROM movie;

ALTER TABLE movie
ADD COLUMN director_id INT NOT NULL;

ALTER TABLE movie
ADD FOREIGN KEY (director_id) REFERENCES director(director_id);

UPDATE movie SET director_id = 1 WHERE director_id = 0;

UPDATE movie SET director_id = 3 WHERE movie_id = 2;
UPDATE movie SET director_id = 1 WHERE movie_id = 3;

-- Requête pour récupérer les infos des films et le nom de leur réalisateur⋅trice
SELECT *
FROM movie m
INNER JOIN director d ON m.director_id = d.director_id
WHERE movie_id = 2;

CREATE TABLE category (
	category_id INT PRIMARY KEY AUTO_INCREMENT NOT NULL,
	category_name VARCHAR(255) NOT NULL
);

CREATE TABLE movie_has_categories (
	-- id INT PRIMARY KEY, si on peut avoir la même categorie plusieurs fois associé au même film
	movie_id INT NOT NULL,
	category_id INT NOT NULL,
	PRIMARY KEY (movie_id, category_id), -- clé composite
	FOREIGN KEY (movie_id) REFERENCES movie(movie_id),
	FOREIGN KEY (category_id) REFERENCES category(category_id)
);

DESC movie_has_categories;

INSERT INTO category (category_name) VALUES
	('Animation'),
	('Horreur'),
	('Fantastique'),
	('Science-Fiction'),
	('Humour');

SELECT * from movie;

-- Alien : Horreur, Science-Fiction
-- Shrek : Animation, Humour, Fantastique
-- erfezf : Fantastique
-- 2	Alien	78	1979	10.0	3
-- 3	Shrek	78	1979	10.0	1
-- 4	zefezf	54	2026	1.0	1
SELECT * FROM category c ;
INSERT INTO movie_has_categories (movie_id, category_id)
VALUES (4, 3);
INSERT INTO movie_has_categories (movie_id, category_id)
VALUES (2, 2), (2, 4);

SELECT * FROM movie_has_categories;

-- Sélectionner les genres (category) du film Alien
SELECT *
FROM category c
INNER JOIN movie_has_categories mhc ON c.category_id = mhc.category_id
INNER JOIN movie m ON mhc.movie_id = m.movie_id
WHERE m.title = 'Alien';